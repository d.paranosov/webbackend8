console.log('Hello');

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('button_change').addEventListener('click', formReceive);
    const form = document.getElementById('userForm');
    form.addEventListener('submit', formSend);

    async function formSend(e) {
        e.preventDefault();

        let error = formValidate(form);

        if (error === 0) {
            let formData = new FormData(form);

            let data = {};

            for(const [key, value] of formData) {
                data[key] = value;
            }

            console.log(data);

            sendData('http://u21391.kubsu-dev.ru/webbackend8/', JSON.stringify(data))
                .then(() => {
                    //form.reset();
                })
                .catch((err) => {
                   console.log(err);
                });
            alert('Отправка выполнена успешно!');
            //document.getElementById('userForm').reset();
        } else {
            alert('Заполните обязательные поля и дайте согласие на обработку персональных данных');
        }
    }

    function formValidate(form) {
        let error = 0;
        let formReq = document.querySelectorAll('._req');

        for (let index = 0; index < formReq.length; index++) {
            const input = formReq[index];
            formRemoveError(input);

            if (input.classList.contains('_email')) {
                if (emailTest(input)) {
                    formAddError(input);
                    error++;
                }
            } else if (input.getAttribute("type") === "checkbox" && input.checked === false) {
                formAddError(input);
                error++;
            } else {
                if (input.value === '') {
                    formAddError(input);
                    error++;
                }
            }
        }
        return error;
    }

    async function formReceive(e) {
        e.preventDefault();

        getData('http://u21391.kubsu-dev.ru/webbackend8/users')
            .catch((err) => {
                console.log(err);
            });
        //alert('Данные получены!');
    }

    function formAddError(input) {
        input.parentElement.classList.add('_error');
        input.classList.add('_error');
    }

    function formRemoveError(input) {
        input.parentElement.classList.remove('_error');
        input.classList.remove('_error');
    }

    function emailTest(input) {
        return !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/.test(input.value);
    }

    const sendData = async(url, data) => {
        const response = await fetch(url, {
            method: 'POST',
            body: data
        });

        if (!response.ok) {
            throw new Error(`Ошибка по адресу ${url}, статус ошибки ${response}`);
        }

        return await response.json();
    };

    const getData = async(url) => {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error(`Ошибка по адресу ${url}, статус ошибки ${response}`);
        }

        console.log(await response.json());
        //return await response.json();
    };

});

//document.getElementById('button_change').addEventListener('click', getPosts);
//document.getElementById('button_admin').addEventListener('click', admin);





const sendUser = () => {

    const userForm = document.querySelector('.userForm');

    const data = {

    }

    userForm.addEventListener('submit', e => {
        e.preventDefault();

        const userList  = JSON.stringify(data);
    });
}

/*
async function getPosts() {
   //let data = document.getElementById('button_change').value;
   console.log(12);
    let response = await fetch('http://u21391.kubsu-dev.ru/webbackend8/users/20');

    if (response.ok) { // если HTTP-статус в диапазоне 200-299
                       // получаем тело ответа (см. про этот метод ниже)
        let json = await response.json();
        console.log(json);
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

async function admin() {
   fetch('http://u21391.kubsu-dev.ru/webbackend8/admin')
       .then((response) => {
          return response.json();
       })
       .then((data) => {
          console.log(data);
       });
}*/

/*document.addEventListener('DOMContentLoaded', () => {

   const ajaxSend = (formData) => {
      fetch('index.php', { // файл-обработчик
         method: 'POST',
         headers: {
            'Content-Type': 'application/json', // отправляемые данные
         },
         body: JSON.stringify(formData)
      })
         .then(response => alert('Сообщение отправлено'))
         .catch(error => console.error(error))
   };

   const forms = document.getElementsByTagName('form');
   for (let i = 0; i < forms.length; i++) {
      forms[i].addEventListener('submit', function (e) {
         e.preventDefault();

         let formData = new FormData(this);
         formData = Object.fromEntries(formData);

         ajaxSend(formData)
            .then((response) => {
               console.log(response);
               form.reset(); // очищаем поля формы
            })
            .catch((err) => console.error(err))
      });
   };
});*/
